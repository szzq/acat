import org.apache.log4j.Logger;

import com.mango.acat.core.server.netty.NettyServer;
import com.mango.acat.core.setting.AppSetting;
import com.mango.acat.core.util.SpringKit;


public class Start{
	public Logger log = Logger.getLogger(getClass());

	public static void main(String[] args) throws Exception {
		/*if(args.length>0){
			System.out.println("参数："+args[0]);
			String arg  = args[0];
			if(arg!=null && arg.equals("productModel")){
				//产品模式
				Constant.contextPath = "";
			}
		}*/
		//先启动spring的容器
		SpringKit.start(new String[]{"spring/spring-dubbo-consumer.xml"});
		new NettyServer().start(AppSetting.app.getInt("app.port"));
		
	}
}