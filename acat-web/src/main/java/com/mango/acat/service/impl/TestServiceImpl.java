package com.mango.acat.service.impl;

import java.util.List;

import org.rex.DB;
import org.rex.RMap;
import org.rex.db.exception.DBException;
import com.mango.acat.service.TestService;

public class TestServiceImpl implements TestService{

	@SuppressWarnings("rawtypes")
	public List<RMap> getList() throws DBException {
		String sql = "SELECT * FROM cms_article";
        List<RMap> list = DB.getMapList(sql,1,10);
        return list;
	}

}
