package com.mango.acat.service;

import java.util.List;

import org.rex.RMap;
import org.rex.db.exception.DBException;

public interface TestService {
	
	@SuppressWarnings("rawtypes")
	public List<RMap> getList() throws DBException;		
}
