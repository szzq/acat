package com.mango.acat.action;

import java.util.List;

import org.rex.RMap;
import org.rex.db.exception.DBException;

import com.alibaba.fastjson.JSONObject;
import com.mango.acat.core.annotation.ReqKey;
import com.mango.acat.core.base.BaseAction;
import com.mango.acat.core.dataset.DataSet;
import com.mango.acat.core.dataset.ParaSet;
import com.mango.acat.core.util.SpringKit;
import com.mango.acat.service.TestService;
@ReqKey(value="/db")
public class DbAction extends BaseAction{
	//dubbo服务的bean注入
	public TestService testService = (TestService) SpringKit.getBean("testService");
	
	@SuppressWarnings("rawtypes")
	public DataSet testList(ParaSet ps) throws DBException{
        List<RMap> list = testService.getList();
		return text(JSONObject.toJSONString(list));
	}
	
}
