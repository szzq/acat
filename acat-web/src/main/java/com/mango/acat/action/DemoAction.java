package com.mango.acat.action;

import com.alibaba.fastjson.JSONObject;
import com.mango.acat.core.annotation.ReqKey;
import com.mango.acat.core.base.BaseAction;
import com.mango.acat.core.dataset.DataSet;
import com.mango.acat.core.dataset.ParaSet;

@ReqKey(value="/demo")
public class DemoAction extends BaseAction{
	
	public DataSet sayHello(ParaSet ps){
		System.out.println(ps.toString());
		return html("public/1.html");
	}
	
	public DataSet post(ParaSet ps){
		System.out.println(ps.toString());
		JSONObject json = new JSONObject();
		json.put("key", 11);
		return json(json);
	}
	
}
