#acat

基于netty打造的一个http java web服务框架。<br/>
（因为netty的异步io机制，能轻松满足一般的并发性能。）<br/>
利用活跃在开源社区中的工具集，快速搭建一款具有学习价值的框架。

* 1.实现`mvc`,自己在netty的基础上实现ReqMap注解。
* 2.实现`orm`,数据库持久化。(集成rexdb，`http://db.rex-soft.org`。)
* 3.实现`ioc`,简单依赖注入（使用spring4的conext的getBean来获取）。
* 4.实现缓存集成。
* 5.实现分布式服务(spring4 + dubbo3)。
* ...

## 项目结构
* acat-parent - 父级别项目
* acat-config - 抽出配置文件到一个子模块中
* acat-core   - 核心项目，netty的http服务器和一些集成工具
* acat-web    - 界面代码，其实就是利用acat的底层来实现自己的业务逻辑。
* acat-web下的home目录下，有一个bin目录，其中是放的wrapper的脚步和运行环境。

## 依赖包
* netty - 能快速实现http服务器
* fastjson - alibaba的json包
* hutool - java工具集
* guava - google的Java工具包
* log4j - 日志包
* okhttp - http的工具包，比apache-common-net好用
* freemarker - Java模板引擎
* wrapper - java wrapper service将多个class的文件打包运行
* spring - ioc容器，集成dubbo服务。
* dubbo - 高速rpc分布式服务框架。

## 快速运行
* 1.开发环境eclipse中运行，运行根目录下的`Start.java`的main方法。
* 1.1.服务器配置文件`app.properties`中。

<img src="./rs/images/appconfig.png"/>

* 2.服务器环境运行，将`acat-web`打包`war`包后解压，运行`bin`下的`./start.sh start`即可(linux上)。
  `startup.bat start` （windows）或者双击`console.bat`运行。

## 2017-05-02
* 增加返回TextDataSet的返回类型。
* 抽出配置文件到acat-config模块中。
* 集成java orm框架rexdb,具体使用请参考rexdb官网文档，`http://db.rex-soft.org/document.php`。
* 集成dubbo，实现简单的分布式服务TestService.`com.mango.demo.DbAction`,使用spring来集成dubbo。
* 使用redis来做dubbo的注册中心，启动服务是需要先启动redis服务。
* 简单测试nginx来反向代理负载均衡。

<img src="./rs/images/acat-nginx-b.png"/>

<img src="./rs/images/acat-nginx.png"/>

<img src="./rs/images/spring-dubbo.png"/>

<img src="./rs/images/spring-dubbo-consumer.png"/>

## 2017-04-28
* 去掉`acat-mvc`这一模块，移动内容到acat-core中。
* 基本实现`session-cookie`的功能（永久，未实现扩展属性，如有效期等）。
* 重新测试通过`wrapper service`在windows上。

## 2017-04-26

* 调整mvc的跳转部分到`BaseAction`中，并完成json的data参数。
* 实现`requestStore`和`sessionStore`的空间存储map,结合实现`BaseAction`的`setAttr`,`getAttr`和`setSessionAttr`,`getSessionAttr`。
* 优化初始化启动acat的输出日志。

<img src="./rs/images/out.png"/>
	   

## 2017-03-26

* 实现netty的http服务器方式。
* 实现请求和响应的类型定义，`DataSet`,`ParaSet`和其子类。
* 实现`html，js,css,jpg`等静态资源请求处理。
* 实现`get,post`请求的参数获取，及页面和json数据的返回。
* 实现静态文件下载，支持浏览器能支持预览的图片文件。
* 实现`freemarker`模板的集成。
* 实现wrapper的打包运行，测试mac(unix),windows通过。
* 将nettyDemo程序，整理到acat的maven项目中。

> 首页截图

<img src="./rs/images/index.png"/>
