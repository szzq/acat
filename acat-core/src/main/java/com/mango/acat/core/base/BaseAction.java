package com.mango.acat.core.base;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.mango.acat.core.dataset.FileDataSet;
import com.mango.acat.core.dataset.FreemarkerDataSet;
import com.mango.acat.core.dataset.HtmlDataSet;
import com.mango.acat.core.dataset.JsonDataSet;
import com.mango.acat.core.dataset.TextDataSet;
import com.mango.acat.core.store.RequestStore;
import com.mango.acat.core.store.SessionStore;

/**
 * 抽象action
 * @author meigang
 *
 */
public abstract class BaseAction {
	/**
	 * 设置存储数据到页面
	 */
	protected void setAttr(String key,Object value){
		RequestStore.setAttr(key, value);
	}
	protected Object getAttr(String key){
		return RequestStore.getAttr(key);
	}
	protected void setSessionAttr(String key,Object value){
		SessionStore.setAttr(key, value);
	}
	protected Object getSessionAttr(String key){
		return SessionStore.getAttr(key);
	}
	
	
	/*
	 * 控制跳转
	 */
	protected HtmlDataSet html(String value){
		return new HtmlDataSet(value);
	}
	
	protected TextDataSet text(String value){
		return new TextDataSet(value);
	}
	
	protected JsonDataSet json(){
		return new JsonDataSet(null);
	}
	
	protected JsonDataSet json(Map<String,Object> data){
		return new JsonDataSet(data);
	}
	
	protected JsonDataSet json(JSONObject data){
		return new JsonDataSet(data);
	}
	
	protected FileDataSet file(String value){
		return new FileDataSet(value);
	}
	
	protected FreemarkerDataSet freemarker(String value) {
		return new FreemarkerDataSet(value);
	}
}
