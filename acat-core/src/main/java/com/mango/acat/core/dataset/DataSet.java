package com.mango.acat.core.dataset;

public class DataSet {
	//网页
	public static final String HTML = "HTML";
	//freemarker
	public static final String FREEMARKER = "FREEMARKER";
	//数据
	public static final String JSON = "JSON";
	//文件下载
	public static final String FILE = "FILE";
	//字符串
	public static final String TEXT = "TEXT";
	
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
