package com.mango.acat.core.dataset;

import java.util.HashMap;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;

public class ParaSet {
	private HashMap<String, Object> data;
	
	public ParaSet(){
		data = Maps.newHashMap();
	}
	
	public void put(String key,String value){
		data.put(key, value);
	}
	
	public String getStr(String key){
		return data.get(key).toString();
	}
	
	public int getInt(String key){
		return Integer.parseInt(getStr(key));
	}
	
	public float getFloat(String key){
		return Float.parseFloat(getStr(key));
	}
	
	@Override
	public String toString() {
		return JSONObject.toJSONString(data);
	}
}
